-----------------------------------------
Welcome to MONSTER MANSION MURDER MASH!
-----------------------------------------
This is a game:
	-For up to eight (8) players
	-Full of mindless violence
	-Based in a mansion
	-Starring Grimgor the Desolator
	-Containing twelve (12) unique characters

What you can do:
	-Move from room to room
	-Fight monsters with a weedwhacker
	-Use a collander as a helmet
	-Get trapped in a pile of books
	-Heal by drinking from a birdbath
	-Heal by eating a potato
	-Open and close doors
	-Get banished to the Nightrealm by Grimgor the Desolator
	
How to win the game:
	Be the last person standing
		OR
	Escape the mansion
		-Find all three pieces of the key
		-Get out through the main gate

Rules:
	

Tips and Tricks:
	Each character has unique strengths and weaknesses
		Bubba: very high health (200), weak hits (20), very slow (5)
			-Mama's boy
		
		George: average health (130), average hits (30), average speed (15)
			-Grocery store supervisor

		Julie: low health (90), strong hits (45), above average speed (20)
			-Amateur stand-up comedian

		Chuck: above average health (150), average hits (30), below average speed (10)
			-Proud member of the NRA
		
		Tyrell: above average health (150), above average hits (35), extremely slow (1)
			-Dumptruck driver for 25 years

		Gustavo: below average health (110), below average hits (25), very fast (30)
			-Pickup soccer champion

		Dr. Fliztofowitz: very low health (75), very strong hits (55), average speed (15)
			-Pediatrician

		Mrs. Li: average health (130), below average hits (25), above average speed (20)
			-Accountant

		Olga von Hammerstein: high health (175), average hits (30), extremely slow (1)
			-Bulgarian powerlifter

		Steve: below average health (110), below average hits (25), below average speed (10)
			-Steve sucks

		Patty: average health (130), strong hits (45), extremely slow (1)
			-Cupcake artist

		Flufferkins: extremely low health (30), extremely weak hits (5), extremely fast (75)
			-Literally just a bunny

	
	Grimgor the Desolator is very powerful
		-All players should avoid him at the beginning of the game
		-Players may need to team up to defeat him
		-He will always have one piece of the key
			-This means he must be killed if any player is to escape

		-Supremely high health: 500
		-Overwhelmingly strong hits: 60
		-No speed whatsoever: 0

	
	What do these stats even do?
		Health is the number of hitpoints a player possesses
			-Hitpoints can be replenished by consuming food and water
			-If the number of hitpoints reaches zero, the player dies
			-The maximum number of hitpoints can be increased by putting on armor
		
		Hits is the measure of a player's attack
			-The number of hits represents the health taken away by a player's attack
			-Hits can be multiplied (or divided) by picking up a weapon
			
		Speed is how agile a player is
			-Speed represents the likelihood of a player dodging an attack


With that said, it's now time for you to enter 
MONSTER MANSION MURDER MASH.

Good luck, adventurer. You'll need it.	
