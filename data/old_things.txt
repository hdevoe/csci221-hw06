/*//Making rooms
	Room* kitchen = new Room("Kitchen", "The stained linoleum is peeling up. Dirty dishes are overflowing the sink.");
	Room* diningRoom = new Room("Dining Room", "A long table stretches from wall to wall. An upturned skull is being used as a soup bowl.");
	Room* pantry = new Room("Pantry", "Expired cans of tomato soup sit on the shelves. A rat crawls around a pile of moldy potatoes.");
	Room* lounge = new Room("Lounge", "A blood-stained rug is curling up along the edges. A dusty recliner sits along the wall.");
	Room* greatHall = new Room("Great Hall", "The front door rattles loosely on its hinges as the rain sweeps in, soaking the mint green carpet.");
	Room* stairs = new Room("Grand Staircase", "A clinking chandelier swings slowly back and forth from the yellowed ceiling. Several steps are missing.");
	Room* den = new Room("Den", "A lamp flickers on and off in the corner. A cobweb-coated skeleton lies in a moldy recliner.");
	Room* library = new Room("Library", "A torn-up pages lie scattered across the floor. A large book entitled 'How to Devour Souls in Six Easy Steps' sits alone on the table");
	Room* tree = new Room("Oak Tree", "A gnarled tree winds up alongside the building. Its outstretched branches reach up to the second floor.");
	Room* driveway = new Room("Driveway", "An old car sits on the gravel, its body riddled with rust. All four tires are missing.");
	Room* fountain = new Room("Fountain", "A gargoyle spits out sludge into a pool of green. Rain splatters down from the night sky.");
	Room* garden = new Room("Garden", "Weeds are strangling the remains of what must once have been rows of potatoes and carrots.");
	Room* shed = new Room("Shed", "Old tools rust from hooks on the wall. The shed smells like soil and gasoline.");
	Room* upstairsHall = new Room("Upstairs Hallway", "The curtains billow as rain blows in from the open window. Lightning crackles."); 
	Room* office = new Room("Office", "A brain sits patiently in a pickle jar on the desk. Butterflies are pinned up in picture frames on the wall.");
	Room* guestRoom = new Room("Guest Room", "A painting of a sad clown sits over the headboard of the bed. There's a signature on the bottom of it: 'From, Bubbles'");
	Room* bathroom =new Room("Bathroom", "A ring of muck lines the inside of the bathtub. A skeleton sits on the toilet, a newspaper in his lap.");	
	Room* masterRoom = new Room("Master Bedroom", "A king-sized mattress lies on top of the crumpled remains of a bedframe. A puddle of blood oozes out from beneath it.");

	
	//Connecting rooms
	gameMap.setExit(kitchen, diningRoom, "east");
	gameMap.setExit(diningRoom, kitchen, "west");
	gameMap.setExit(kitchen, pantry, "north");
	gameMap.setExit(pantry, kitchen, "south");
	gameMap.setExit(kitchen, lounge, "south");
	gameMap.setExit(lounge, kitchen, "north");
	gameMap.setExit(lounge, greatHall, "west");
	gameMap.setExit(greatHall, lounge, "east");	
	gameMap.setExit(greatHall, stairs, "north");
	gameMap.setExit(stairs, greatHall, "south");
	gameMap.setExit(greatHall, den, "west");
	gameMap.setExit(den, greatHall, "east");	
	gameMap.setExit(den, library, "north");
	gameMap.setExit(library, den, "south");
	gameMap.setExit(library, tree, "west");
	gameMap.setExit(tree, library, "east");
	gameMap.setExit(greatHall, driveway, "south");
	gameMap.setExit(driveway, greatHall, "north");
	gameMap.setExit(driveway, fountain, "east");
	gameMap.setExit(fountain, driveway, "west");
	gameMap.setExit(fountain, garden, "north");
	gameMap.setExit(garden, fountain, "south");
	gameMap.setExit(shed, garden, "south");
	gameMap.setExit(garden, shed, "north");
	gameMap.setExit(garden, diningRoom, "west");
	gameMap.setExit(diningRoom, garden, "east");
	gameMap.setExit(stairs, upstairsHall, "east");
	gameMap.setExit(upstairsHall, stairs, "west");
	gameMap.setExit(stairs, office, "west");
	gameMap.setExit(office, stairs, "east");
	gameMap.setExit(office, tree, "north");
	gameMap.setExit(tree, office, "south");
	gameMap.setExit(office, guestRoom, "south");
	gameMap.setExit(guestRoom, office, "north");
	gameMap.setExit(guestRoom, bathroom, "east");
	gameMap.setExit(bathroom, guestRoom, "west");
	gameMap.setExit(bathroom, masterRoom, "east");
	gameMap.setExit(masterRoom, bathroom, "west");
	gameMap.setExit(masterRoom, upstairsHall, "north");
	gameMap.setExit(upstairsHall, masterRoom, "south");

	//Adding bags
	Bag * car = new Bag("car", "a rusted out car resting on cinder blocks");
	driveway->addThing(car);
	//Adding weapons
	Weapon * ladle = new Weapon(1.1, "ladle", "a large wooden spoon");*/
	/*Weapon * claws = new Weapon(3.0, "claws", "Grimgor's own claws");
	Weapon * cane = new Weapon(1.3, "cane", "good for whipping whippersnappers");
	Weapon * wrench = new Weapon(1.5, "wrench", "covered in grease");	
	Weapon * umbrella = new Weapon(1.2, "umbrella", "dripping wet");
	Weapon * club = new Weapon(1.4, "club", "a rock tied to the end of a stick");
	greatHall->addThing(umbrella);
	kitchen->addThing(ladle);
	lounge->addThing(cane);
	car->addThing(wrench);	

	//Adding food
	Food * soupCan = new Food(15, "soup", "a rusty can of tomato soup");
	Food * potato = new Food(20, "potato", "a moldy potato");
	
	pantry->addThing(soupCan);
	pantry->addThing(potato);

	//Adding armor
	Armor * collander = new Armor(10, "collander", "head");
	kitchen->addThing(collander);

*/	
	//Creating characters	
	/*charList.push_back(character(200, 20, 20, "Bubba", "Mama's boy"));
	charList.push_back(character(130, 30, 6, "George", "Grocery store supervisor"));
	charList.push_back(character(90, 45, 6, "Julie", "Amateur stand-up comic"));
	charList.push_back(character(150, 30, 10, "Chuck", "Proud member of the NRA"));
	charList.push_back(character(150, 35, 100, "Tyrell", "Dumptruck driver"));
	charList.push_back(character(110, 25, 3, "Gustavo", "Pickup soccer champion"));
	charList.push_back(character(75,55,6, "Fliztofowitz", "Pediatrician"));
	charList.push_back(character(130, 25, 5, "Li", "Accountant"));
	charList.push_back(character(175, 30, 100, "Olga", "Bulgarian Powerlifter"));
	charList.push_back(character(110, 25, 10, "Steve", "Steve sucks"));
	charList.push_back(character(130, 45, 100, "Patty", "Cupcake artist"));
	charList.push_back(character(30, 5, 2, "Flufferkins", "Literally just a bunny"));*/
	

