#ifndef CHARACTERS_H
#define CHARACTERS_H
#include <string>

class character
{
	public:
	//Constructor
	character(int hp, int pow, int spd, std::string title, std::string descr);

	//Vars
	int health;
	int hits;
	int speed;
	std::string name;
	std::string description;	
};

#endif
