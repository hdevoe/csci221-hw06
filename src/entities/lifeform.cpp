#include "lifeform.h"
#include <iostream>
#include <stdlib.h>


void Lifeform::attack(Lifeform* target) {
	int dodge = (int)rand()%100;
	int speed = target->getSpeed();
	if(dodge%speed!=0){
		target->setHealth(-1*((int)(hits+myWeapon->getDamage())));
		if(target->getHealth() <=0) target->Kill();
		std::cout<<std::endl<<longName<<" attacks "<<target->getName()<<" for " << (int)hits + myWeapon->getDamage()<<" damage!";
	} else std::cout<<std::endl<<target->getName()<< " dodged the attack!";
}

int Lifeform::getSpeed() {
	return speed;
}

void Lifeform::move(Room* direction) {
	direction->addThing(this);
	currentRoom->removeThing(longName);
	currentRoom = direction;
}

void Lifeform::pickup(Thing* item) {
	inv.addThing(item);
}

void Lifeform::drop(std::string description) {
	return inv.removeThing(description);
}

bool Lifeform::isAlive() {
	return alive;
}

void Lifeform::Kill() {
	for(int i = 0; i < inv.getThings().size();i++) {
		currentRoom->addThing(inv.getThings()[i]);
	}
	for(int i = 0; i < inv.getThings().size();i++) {
		drop(inv.getThings()[i]->getName());
	}
	alive = false;
}

int Lifeform::getHealth() {
	return health;
}

int Lifeform::getMaxHealth() {
	return maxHealth;
}

int Lifeform::getHits() {
	return hits;
}

void Lifeform::setHealth(int hp) {
	health += hp;
	if (health >= maxHealth) health = maxHealth;
}

void Lifeform::setMaxHealth(int hp) {
	maxHealth += hp;	
	if(health >= maxHealth) health = maxHealth;
}

Room* Lifeform::getCurrentRoom() {
	return currentRoom;
}

std::string Lifeform::getCurrentRoomName() {
	return currentRoom->getName();
}

Bag Lifeform::getInventory() {
	return inv;
}

Weapon* Lifeform::getWeapon() {
	return myWeapon;
}

void Lifeform::setWeapon(Weapon* newWeapon) {
	myWeapon = newWeapon;
}
