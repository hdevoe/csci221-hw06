#ifndef LIFEFORM_H
#define LIFEFORM_H
#include <iostream>
#include "../items/container.h"
#include "../items/armor.h"
#include <vector>
#include "../items/weapon.h"
#include "../items/bag.h"
#include "../rooms/room.h"
#include <stdlib.h>
class Lifeform : public Thing
{
	public:
	//methods
	Lifeform(int &hp, int &pow, int &spd, std::string &id, Room* loc) {
		alive = true;
		maxHealth = hp;
		health = maxHealth;
		hits = pow;
		speed = spd;
		longName = id;
		currentRoom = loc;		
		//currentRoom->addThing(*this);
	}	
	void attack(Lifeform* target);
	void move(Room* direction); 
	void pickup(Thing* item);
	void drop(std::string description);
	bool isAlive();
	void Kill();
	int getHealth();
	int getMaxHealth();
	void setHealth(int);
	void setMaxHealth(int);
	Room* getCurrentRoom();
	std::string getCurrentRoomName();
	Bag getInventory();
	Weapon* getWeapon();
	void setWeapon(Weapon* newWeapon);
	int getHits();
	int getSpeed();
	//Desctructor
	~Lifeform() {
	//	delete currentRoom;	
	}	
	protected:
	//vars
	bool alive;
	int maxHealth;
	int health;
	int hits;
	int speed;
	Weapon* myWeapon;
	Bag inv;
	Room* currentRoom;

};
#endif
