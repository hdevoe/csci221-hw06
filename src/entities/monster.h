#ifndef MONSTER_H
#define MONSTER_H
#include "lifeform.h"

class Monster : public Lifeform
{
	public:
	Monster(int &hp, int &pow, int &spd, std::string &id, Room* loc):Lifeform(hp,pow,spd,id,loc) { 
                alive = true;                                                                                         
                maxHealth = hp;                                                                                       
                health = maxHealth;                                                                                   
                hits = pow;                                                                                           
                speed = spd;                                                                                          
                longName = id;                                                                                        
                currentRoom = loc;                                                                                    
                currentRoom->addThing(this);                                                                         
                ID = 3;                                                                                               
        }         

	using Lifeform::move;
	void move();
	
};
#endif
