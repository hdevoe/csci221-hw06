#ifndef PLAYER_H
#define PLAYER_H
#include "lifeform.h"

class Player : public Lifeform
{
	public:
	Player(int &hp, int &pow, int &spd, std::string &id, Room* loc):Lifeform(hp,pow,spd,id,loc) {
		alive = true;
		maxHealth = hp;
		health = maxHealth;
		hits = pow;
		speed = spd;
		longName = id;
		currentRoom = loc;
		currentRoom->addThing(this);
		ID = 2;	
	}
};

#endif
