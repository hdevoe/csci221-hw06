#ifndef EXIT_H
#define EXIT_H

#include "items/thing.h"

#include <iostream>

class Exit : public Thing
{
	public:
	Exit(double mult, std::string id, std::string info):Thing(mult, id, info) {
		longName = id;
		description = info;
		myMult = mult;
		ID = 4;	
	}	
	double getMult()
	{
		return myMult;	
	}
	private: 
	double myMult;

};

#endif
