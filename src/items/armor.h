#ifndef ARMOR_H
#define ARMOR_H

#include "thing.h"
#include <iostream>
class Armor : public Thing{
	
	public:
	 Armor(double mult, std::string id, std::string info):Thing(mult, id, info) {
		longName = id;
		description = info;
		armor = mult;
		ID = 6;
	}
	double getArmor()
	{
		return armor;
	}
	private:
	double armor;

};
#endif
