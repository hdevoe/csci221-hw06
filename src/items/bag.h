#ifndef BAG_H
#define BAG_H

#include "container.h"
#include "thing.h"
#include <iostream>

class Bag : public Container, public Thing
{
	public:
	Bag(std::string name, std::string descr) : Container(name, descr)
	{
		longName = name;
		description = descr;
		ID = 1;
	}
	Bag():Container() {}
	using Container::Container;
	using Container::listThings;	
};

#endif
