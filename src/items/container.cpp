#include "container.h"
#include <algorithm>
#include <cstddef>
#include <iostream>
#include "weapon.h"
#include "../entities/player.h"
#include "../entities/monster.h"
#include "food.h"
#include "armor.h"

using std::swap;
using std::find_if;
using std::transform;


std::vector<Thing*> Container::getThings()
{
	return thingsInContainer;	
}

void Container::addThing(Thing* object) {
	thingsInContainer.push_back(object);
}

void Container::removeThing(std::string name) {
	for (int i = 0; i < thingsInContainer.size();i++) 
	{
		if(name == thingsInContainer[i]->getName())
		{
    			swap(thingsInContainer[i], thingsInContainer.back());
    			thingsInContainer.pop_back();
		}
	}
}

Thing* Container::returnThing(std::string name) {
	for (int i = 0; i < thingsInContainer.size();i++)
	{
		if (name == thingsInContainer[i]->getName())
		{
			return thingsInContainer[i];
		}
	}
}
int Container::checkThing(std::string name) {
	for (int i = 0; i < thingsInContainer.size();i++)
	{
		if(thingsInContainer[i]->getName() == name) return thingsInContainer[i]->ID;
	}	
	return -1;
}
std::vector<Thing*> Container::getContainer() {
	return thingsInContainer;	
}

void Container::listThings()
{
	std::cout<<std::endl<<std::endl<< "Contains: ";
	for(int i = 0; i <thingsInContainer.size();i++)
	{
		std::cout<<std::endl<<std::endl<<thingsInContainer[i]->getName();
		int ident = thingsInContainer[i]->ID;
		if(ident == 2) std::cout<<" (player) ";
		if(ident == 3) std::cout<<" (monster) ";
		if(ident ==2|| ident==3) {
			Lifeform* tempLifer = (Lifeform*)thingsInContainer[i];
			
			if(tempLifer->isAlive()){
				std::cout<< "health: " << tempLifer->getHealth();
			} else std::cout<<" (dead)";
		}
		if(ident == 1) std::cout<< " (container) ";
		if(ident == 4) {
			Weapon* tempWeapon = (Weapon*)thingsInContainer[i];
				std::cout<< " (weapon) attack: " <<tempWeapon->getDamage(); 
		} 
		if(ident == 5) {
			Food* tempFood = (Food*)thingsInContainer[i];
				std::cout<<" (food) health: " << tempFood->getBonus();
		}
		if(ident != 2 && ident != 3) std::cout<<std::endl<<"	-" <<thingsInContainer[i]->getDescr();
	}
}
