#ifndef CONTAINER_H
#define CONTAINER_H


#include "thing.h"


class Container {
    protected:
        std::vector<Thing*> thingsInContainer;
	public:
	Container(std::string name, std::string descr) {}
	Container(){}
	std::vector<Thing*> getThings();
	void addThing(Thing*);
	Thing* returnThing(std::string name);
	void removeThing(std::string name);
	std::vector<Thing*> getContainer();
	int checkThing(std::string name);
	void listThings();
};

#endif

