#ifndef FOOD_H
#define FOOD_H

#include "thing.h"
#include <iostream>
class Food : public Thing{
	
	public:
	 Food(int heal, std::string id, std::string info):Thing(heal, id, info) {
		longName = id;
		description = info;
		bonus = heal;
		ID = 5;
	}
	int getBonus()
	{
		return bonus;
	}
	private:
	int bonus;

};
#endif
