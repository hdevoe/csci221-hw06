#ifndef KEY_H
#define KEY_H
#include "thing.h"
#include <iostream>

class Key : public Thing
{
	public:
	Key(double mult, std::string id, std::string info):Thing(mult, id, info) {
		longName = id;
		description = info;
		myMult = mult;
		ID = 7;	
	}	
	double getMult()
	{
		return myMult;	
	}
	private: 
	double myMult;

};

#endif
