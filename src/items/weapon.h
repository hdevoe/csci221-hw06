#ifndef WEAPON_H
#define WEAPON_H
#include "thing.h"
#include <iostream>

class Weapon : public Thing
{
	public:
	Weapon(int damage, std::string id, std::string info):Thing(damage, id, info) {
		longName = id;
		description = info;
		myDamage = damage;
		ID = 4;	
	}	
	int getDamage()
	{
		return myDamage;	
	}
	private: 
	int myDamage;

};

#endif
