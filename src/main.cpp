using namespace std;
#include "parser.h"
#include "entities/character.h"
#include "items/thing.h"
#include "entities/player.h"
#include "items/armor.h"
#include "entities/monster.h"
#include "items/container.h"
#include "rooms/room.h"
#include "items/bag.h"
#include "rooms/gamemap.h"
#include "entities/lifeform.h"
#include <vector>
#include <iostream>
#include <stdlib.h>
#include "items/key.h"
#include "exit.h"

#include "reader.h"


vector<Lifeform*> gameIntro(vector<character> charList, vector<Lifeform*> playerList, GameMap gameMap)
{

	//Introducing the game
	bool correctNum = false;
	int players;
	while(!correctNum) {
		cout << endl<<endl<<endl<<"Welcome to MONSTER MANSION MURDER MASH!" << endl;
		cout << "How many people are playing? (Up to 12): ";
		cin >> players;
		if(players > 0 && players <= 12) {
			correctNum = true;
		} else cout<<endl<<"Please pick a number between 1 and 12.";
	}
	
	//Character select loop
	for (int i = 0; i < players; i++)
	{
		bool unselected = true;
		while (unselected)
		{
			bool recognizable = false;
			cout <<endl<<"(Type list for characters)" << endl;
			cout << "Player " << i+1 <<", select your character: ";
			std::string word;
			cin >> word;
			cout << endl;
		
			if (word == "list" || word == "ls") 
			{
				for (int j = 0; j < charList.size(); j++)
				{
					cout << charList[j].name<<"- Health: " <<charList[j].health<<" Hit Power: ";
					cout << charList[j].hits << " Speed: " << 100 / charList[j].speed<<endl;
					cout << "	" <<charList[j].description<<endl<<endl;
					recognizable = true;
				}	

			} else {
				for (int t = 0; t < charList.size(); t++)
				{
					if (word == charList[t].name ) 
					{
						bool taken = false;
						for (int x = 0; x < playerList.size(); x++)
						{
							if (word == playerList[x]->getName())
							{
								cout << "Character already selected!" << endl;
								recognizable = true;
								taken = true;
							}
						}
						
						if (!taken) 
						{
							playerList.push_back(new Player(charList[t].health, charList[t].hits, charList[t].speed, charList[t].name,gameMap.getRooms()[0]));
							recognizable = true;	
							cout << charList[t].name << " selected."<<endl; 
						unselected = false;
						}
					}
				}
			} 	
			if (!recognizable) cout <<"Command not recognized"<< endl;
		}
	}	

	return playerList;
	
}


void printOpening(vector<Lifeform*> playerList)
{
	cout<<endl<<endl<<endl<<endl; 
	cout<< "Now it's time to start MONSTER MANSION MURDER MASH!"<<endl<<endl<<endl<<endl;
	cout<<"It is a DARK and STORMY night."<<endl;
	cout<<"Lightning crackles, wolves howl, and " << playerList.size()-1<< " strangers find themselves";
	cout<<endl<<"torn from their daily lives and thrown into a strange house";
	cout<<endl<<"full of horror and mystery.";
	cout<<endl<<"However, they are not alone...";
	cout<<endl<<"A strange beast lurks deep within the halls of Monster Mansion,";
	cout<<endl<<"grumbling and stumbling from room to room, seeking";
	cout<<endl<<"any soul unlucky enough to stumble upon him."<<endl<<endl;	
}


int main()
{
	
	Reader reader;
	GameMap gameMap;

	gameMap = reader.readGameMap("../data/gamemaps/test.txt", gameMap);


	vector<character> charList;
	vector<Lifeform*> playerList;
	vector<Lifeform*> monsterList;


	
	Weapon * hands = new Weapon(1.0, "hands", "you should probably find a weapon soon");
 		


	charList = reader.readCharList("../data/characters/test.txt");
	playerList = gameIntro(charList, playerList, gameMap);

	



	monsterList = reader.readMonsterList("../data/monsters/test.txt", gameMap);
	gameMap = reader.randEntityLoc(monsterList, gameMap);
	


	


	Parser p = Parser(gameMap);


	printOpening(playerList);


	for (int t = 0; t < playerList.size()-1; t++)
	{
		playerList[t]->setWeapon(hands);	
	}
	bool running = true;
	
	while (running) {
		for (int t = 0; t < playerList.size();t++)
		{
			if(playerList[t]->isAlive()) {
				running = p.readInput(playerList[t], t+1);	
			}
		}
		
		int chance = rand();
		for(int t = 0; t < monsterList.size();t++)
		{
			cout<<endl<<monsterList[t]->getName()<<" is moving..."<<endl;

			Monster* tempMonster = (Monster*) monsterList[t];
			tempMonster->move();
		}
		
	}
	




	for(int i = 0; i < playerList.size(); i++) {
		delete playerList[i];
	}

	delete hands;
	
	gameMap.clearGameMap();
    return 0;
}


