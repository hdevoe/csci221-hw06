#include "parser.h"
#include "entities/lifeform.h"
#include "items/container.h"
#include "items/armor.h"
#include "rooms/room.h"
#include <iostream>
using namespace std;
Parser::Parser(GameMap map)
{
	myMap = map;
}

bool Parser::readInput(Lifeform* player, int index) {

	//outputting current room details
	cout<<endl<<"Location: " << player->getCurrentRoom()->getName();
	cout<<endl<<player->getCurrentRoom()->getDescription();
	player->getCurrentRoom()->listThings();
	cout<<endl;	
	//outputting surrounding room details
	if(myMap.getExit(player->getCurrentRoomName(), "north"))
	{
		cout<<endl<<"To the north is the " <<  myMap.getExit(player->getCurrentRoomName(),"north")->getName();
	}

	if(myMap.getExit(player->getCurrentRoomName(), "south"))
	{
		cout<<endl<<"To the south is the " <<  myMap.getExit(player->getCurrentRoomName(),"south")->getName();
	}

	if(myMap.getExit(player->getCurrentRoomName(), "east"))
	{
		cout<<endl<<"To the east is the " <<  myMap.getExit(player->getCurrentRoomName(),"east")->getName();
	}

	if(myMap.getExit(player->getCurrentRoomName(), "west"))
	{
		cout<<endl<<"To the west is the " <<  myMap.getExit(player->getCurrentRoomName(),"west")->getName();
	}
	
	bool moved = false;
	while(!moved)
	{
		cout << endl<< player->getName() << " (P" << index<<")-> ";
		string verb;
		cin >> verb;
		if(verb == "help") {
			cout << endl<<endl<<"HELP: ";
			cout<<endl<<endl<<endl<<"OBJECTIVE OF THE GAME: ";
			cout<<endl<<"Find the three (3) key parts and escape MONSTER MANSION!";
			cout<<endl<<"Hint: Grimgor the Desolator will always have a part of the key.";
			cout<<endl<<"NOTE: this objective has not been implemented yet." ;
			cout<<endl<<"Therefore, as of now, the last one standing wins!";

			cout<<endl<<endl<<endl<<"PARSER: ";
			cout<<endl<< "Issue most commands by using the syntax <verb> <item>.";
			cout<<endl<< "List of verbs: ";
			cout<<endl<<endl<< "go <direction>";
			cout<<endl<< "	(north, south, east, or west)";
			cout<<endl<<endl<< "hit <lifeform>";
			cout<<endl<< "	(player or Grimgor)";
			cout<<endl<<endl<< "get <item>";
			cout<<endl<<"	key part, weapon, armor, health item)";
			cout<<endl<<endl<<"open <container>";
			cout<<endl<<endl<<"eat <food item from inventory>";
			cout<<endl<<endl<<"drop <item from inventory>";
			cout<<endl<<endl<<"look";
		} else if(verb == "quit") {
			return false;
		} else if(verb == "go") {
			string direction;
			cin >> direction;
			bool foundDirection = false;
			if(myMap.getExit(player->getCurrentRoomName(), "north") && direction == "north")
			{
				moved = foundDirection = true;	
				player->move(myMap.getExit(player->getCurrentRoomName(), direction));
			}

			if(myMap.getExit(player->getCurrentRoomName(), "south") && direction == "south")
			{
				moved = foundDirection = true;
				player->move(myMap.getExit(player->getCurrentRoomName(), direction));
			}

			if(myMap.getExit(player->getCurrentRoomName(), "east") && direction == "east")
			{
				moved = foundDirection = true;
				player->move(myMap.getExit(player->getCurrentRoomName(), direction));
			}

			if(myMap.getExit(player->getCurrentRoomName(), "west") && direction == "west")
			{
				moved = foundDirection = true;
				player->move(myMap.getExit(player->getCurrentRoomName(), direction));
			}
			if (foundDirection)
			{
				cout << "You moved " << direction << endl;	
			} else cout << "Direction not available"<<endl;
		} else if(verb == "get") {
			string noun;
			cin >> noun;
			int num = player->getCurrentRoom()->checkThing(noun);
			bool itemExists = false;	
			if(num == 4)
			{		
				player->pickup(player->getCurrentRoom()->returnThing(noun));
				if (player->getWeapon()->getName()!="hands")
				{
					player->getCurrentRoom()->addThing(player->getWeapon());
				}
				player->drop(player->getWeapon()->getName());
				player->setWeapon((Weapon*)player->getCurrentRoom()->returnThing(noun));
				player->getCurrentRoom()->removeThing(noun);
				itemExists = moved = true;	
			} else if (num == 5)
			{
				player->pickup(player->getCurrentRoom()->returnThing(noun));
				player->getCurrentRoom()->removeThing(noun);
				cout<<endl<<"Picked up " << noun << ".";
				itemExists = moved = true;
			} else if (num == 6)
			{
				cout<<endl<<"You put the " << noun <<" on your " << player->getCurrentRoom()->returnThing(noun)->getDescr()<<".";
				Armor* tempArmor = (Armor*)player->getCurrentRoom()->returnThing(noun);
				player->setMaxHealth((int)tempArmor->getArmor());				
				player->pickup(player->getCurrentRoom()->returnThing(noun));
				player->getCurrentRoom()->removeThing(noun);
				itemExists = moved = true;	
			}	
			if(!itemExists) cout << endl<<noun << " not found.";		
		} else if(verb == "hit") {
			string noun;
			cin >> noun;
			int num = player->getCurrentRoom()->checkThing(noun);
			if(num == 2 || num == 3)
			{
				if(player->getName()!=noun)
				{
					player->attack((Lifeform*)player->getCurrentRoom()->returnThing(noun));
					moved = true;	
				} else cout<< endl<<"Stop hitting yourself!";
			} else if (num != -1)
			{
				cout<< endl<<"Cannot attack " << noun<<"!";
			} else cout <<endl<<"Target not found.";
		
		} else if (verb == "inv" || verb == "status" || verb == "inventory" || verb == "bag") {
			cout<<endl<<"STATUS: "<<endl;
			cout<<endl<<"Max Health: " << player->getMaxHealth();
			cout<<endl<<"Current Health: " << player->getHealth();
			cout<<endl<<"Current Weapon: " <<player->getWeapon()->getName()<< " (" << player->getWeapon()->getDamage() << ")";
			cout<<endl<<"Current Damage: " <<player->getHits() << " * " << player->getWeapon()->getDamage();
			
			cout<<endl<< "INVENTORY: " <<endl;
			for (int i = 0; i < player->getInventory().getContainer().size();i++) {
				cout << endl<< player->getInventory().getContainer()[i]->getName() << "- ";
				cout << player->getInventory().getContainer()[i]->getDescr();
			}
		} else if(verb == "drop") {
			string noun;
			cin >> noun;
			int num  = player->getInventory().checkThing(noun);
			if (num == 4)
			{
				cout<<endl<<"Can only drop your weapon when you pick up another weapon!";
			} else if (num!= -1)
			{
				if(num == 6) 
				{
					Armor* tempArmor = (Armor*)player->getInventory().returnThing(noun);
					player->setMaxHealth((int)tempArmor->getArmor()*-1);
				}
				player->getCurrentRoom()->addThing(player->getInventory().returnThing(noun));
				player->drop(noun);
				cout<<endl<<"Dropping " << noun<<".";
			} else {
				cout<<endl<<noun<<" not found in inventory.";	
			}
		} else if(verb == "eat") {
			string noun;
			cin>>noun;
			int num = player->getInventory().checkThing(noun);
			if (num == 5)
			{
				tempFood = (Food*)player->getInventory().returnThing(noun);
				player->setHealth((int)tempFood->getBonus());
				player->drop(noun);
				cout<<"Ate " << noun<<".";
			}else {
				if (num!=-1)
				{
					cout<<endl<<"Cannot eat " << noun<<"!";
				} else cout<<endl<<noun<<" not found in inventory";
			}
		} else if(verb == "look"||verb == "ls"||verb == "room") {
			cout<<endl<<"Location: " << player->getCurrentRoomName();
			cout<<endl<<player->getCurrentRoom()->getDescription();

			player->getCurrentRoom()->listThings();
				
			cout<<endl;	
			//outputting surrounding room details
			if(myMap.getExit(player->getCurrentRoomName(), "north"))
			{
				cout<<endl<<"To the north is the " <<  myMap.getExit(player->getCurrentRoomName(),"north")->getName();
			}

			if(myMap.getExit(player->getCurrentRoomName(), "south"))
			{
				cout<<endl<<"To the south is the " <<  myMap.getExit(player->getCurrentRoomName(),"south")->getName();
			}

			if(myMap.getExit(player->getCurrentRoomName(), "east"))
			{
				cout<<endl<<"To the east is the " <<  myMap.getExit(player->getCurrentRoomName(),"east")->getName();
			}

			if(myMap.getExit(player->getCurrentRoomName(), "west"))
			{
				cout<<endl<<"To the west is the " <<  myMap.getExit(player->getCurrentRoomName(),"west")->getName();
			}

		} else if (verb == "open") {
			string noun;
			cin>>noun;	
			int num  = player->getCurrentRoom()->checkThing(noun);
			if(num == 1) {
				bool inBag = true;
				cout<<endl<<"Opening "<<noun<<".";
				Bag* currentBag = (Bag*)player->getCurrentRoom()->returnThing(noun);	

				while(inBag) 	{
					
					currentBag->listThings();
					cout<<endl<<"(Type exit to return to room, else type get <item>)";
					cout<<endl<<player->getName()<<" (P"<< index<<")->";
					string command;
					cin>>command;
					if(command == "exit") {
						cout<<endl<<"Exiting " << noun<<".";
						inBag = false;
					} else if(command == "get") {
						string bagItem;
						cin>>bagItem;
						int bagNum  = currentBag->checkThing(bagItem);
						bool itemExists = false;
					
						if(bagNum == 4)
						{
									
							player->pickup(currentBag->returnThing(bagItem));
							if (player->getWeapon()->getName()!="hands")
							{
								currentBag->addThing(player->getWeapon());
							}
							player->drop(player->getWeapon()->getName());
							player->setWeapon((Weapon*)currentBag->returnThing(bagItem));
							currentBag->removeThing(bagItem);
							itemExists = moved = true;
							inBag = false;	
						} else if (bagNum == 5)
						{
							player->pickup(currentBag->returnThing(bagItem));
							currentBag->removeThing(bagItem);
							cout<<endl<<"Picked up " << bagItem << ".";
							itemExists = moved = true;
							inBag = false;
						} else if (bagNum == 6)
						{
							cout<<endl<<"You put the " << bagItem <<" on your " << currentBag->returnThing(bagItem)->getDescr()<<".";
							Armor* tempArmor = (Armor*)currentBag->returnThing(bagItem);
							player->setMaxHealth((int)tempArmor->getArmor());				
							player->pickup(currentBag->returnThing(bagItem));
							currentBag->removeThing(bagItem);
							itemExists = moved = true;
							inBag = false;	
						}else if (bagNum = -1) cout<<endl<<bagItem<<" not found";	
					} 
				}
			}

		}
	}
    return true;
}

