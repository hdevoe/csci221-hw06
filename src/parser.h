#ifndef PARSER_H
#define PARSER_H

#include "items/container.h"
#include "items/armor.h"
#include "items/food.h"

#include "entities/lifeform.h"
#include "entities/lifeform.h"

#include "rooms/room.h"
#include "rooms/gamemap.h"

#include <iostream>

using namespace std;


class Parser {
	public:
		Parser(GameMap map);
        	bool readInput(Lifeform* player, int index);
		~Parser() {
			//delete tempLife;
		}
	private:
		GameMap myMap;
		Lifeform* tempLife;
		Food* tempFood;	
};

#endif

