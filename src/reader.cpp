#include "reader.h"

Reader::Reader()
{

}



//Read the Game Map (Create rooms, map and items)
GameMap Reader::readGameMap(string mapFile, GameMap gameMap)
{

	//Stream-In input file
	string fileName = mapFile;	
	ifstream infile(fileName.c_str());
	
	string line;


	//Initiate handling vars
	bool roomCycle = false;
	bool mapCycle = false;
	bool itemCycle = false;
	
	//Cycle through file to end of file 
	while (getline(infile, line))
	{
		//create an istringstream of line
    		istringstream iss(line);
	
		
		if(!roomCycle && !mapCycle && !itemCycle)
		{
			string tempName;
			if(iss>>tempName && tempName == "Rooms:") 
			{
				roomCycle = true;
			} else cout<<"Error: unable to read file"<<endl<<"Check formatting."<<endl;
		} else if (roomCycle)
		{
			
			string tempName;			
			
			//error if word at beginning of line doesn't exist
			if (!(iss >> tempName)) 
			{
	
					
				cout<<"Error: unable to read file"<<endl;	
	 			break;
	

			//check if first word in line is 'Map:' and if so set Map Cycle to true and room Cycle to false	
			} else if (tempName == "Map:")
			{
				mapCycle = true;
				roomCycle = false;

			//If first word in line is name, begin creating room
			} else if (tempName == "Name:")
			{
				
				//initiate vars for room creation
				bool name = true;
				string roomName, roomDesc, tempPart;
					

				//cycle through line until line ends
				while(iss>>tempPart) {
					//if next string is 'Desc:' begin creating description of room
					if(tempPart == "Desc:")
					{
						name = false;

					//if name bool is true, add words to room name
					} else if (name) 
					{
						roomName = roomName + tempPart + " ";

					//if name bool is false, add words to room description
					} else if (!name) roomDesc = roomDesc + tempPart + " ";
				}

				//once line is cycled, create a room with given room name and description
				gameMap.makeRoom(roomName, roomDesc);	

			}


		} else if (mapCycle)
		{
		
			string tempName;			
			
			//error if word at beginning of line doesn't exist
			if (!(iss >> tempName)) 
			{
		
				cout<<"Error: unable to read file"<<endl;	
	 			break;
			//Begins Item Cycle if "Items:" appears
			} else if (tempName == "Items:")
			{
				itemCycle = true;
				mapCycle = false;
				roomCycle = false;
			}else if (tempName == "From:" && mapCycle)
			{

			bool from = true;
			bool to = false;
			string fromRoom, toRoom, direction, tempPart;

				while(iss>>tempPart) 
				{
		
					if(tempPart == "To:")
					{
						from = false;
						to = true;
					} else if (tempPart == "Dir:" && !from && to)
					{
						
						to = false;		
				
					} else if (from)
					{
						fromRoom = fromRoom + tempPart+ " ";
					} else if (to)
					{
						toRoom = toRoom + tempPart+ " ";
					} else if (!from && !to) direction = tempPart;		
				}

			gameMap.setExit(fromRoom, toRoom, direction);		
		
			}
		
		} else if (itemCycle)
		{

			string tempName;			
			
			//error if word at beginning of line doesn't exist
			if (!(iss >> tempName)) 
			{
				cout<<"Error: unable to read file"<<endl;	
	 			break;

			//If line starts with 'Bag:', create bag with name + description
			} else if (tempName == "Bag:")
			{

				//Bag requires name, description, and location (room or other bag)
				string bagName, bagDescription,bagLoc, tempPart;
				bool name = true;
				bool desc = false;

				//Cycle through line until line ends
				while(iss>>tempPart) {
				
					if(tempPart == "Desc:")
					{
						name = false;
						desc = true;
					} else if (tempPart == "Loc:" && !name && desc)
					{
						
						desc = false;
					} else if (name)
					{

						bagName = bagName + tempPart + " ";

					} else if (desc) 
					{
						bagDescription = bagDescription + tempPart + " ";
					} else if (!name && !desc) bagLoc = bagLoc + tempPart + " ";


				}

				//Create bag with name and description
				Bag * tempBag = new Bag(bagName, bagDescription);
				gameMap.getRoomByName(bagLoc)->addThing(tempBag);	
				gameMap.getThings().push_back(tempBag);				
		
			//Create weapon if line starts with weapon
			} else if (tempName == "Weapon:")
			{
				///weapon requires name, description, damage, and location (room or other bag)
				string weapName, weapDescription,weapLoc, weapDam, tempPart;
				bool name = true;
				bool desc = false;
				bool loc = false;

				//Cycle through line until line ends
				while(iss>>tempPart) {
				
					if(tempPart == "Desc:")
					{
						name = false;
						desc = true;
						loc = false;
					} else if (tempPart == "Loc:" && !name && desc && !loc)
					{
						loc = true;
						name = false;
						desc = false;
					} else if (tempPart == "Dam:" && !name && !desc && loc)
					{

						loc = false;
						
					} else if (name)
					{

						weapName = tempPart;

					} else if (desc) 
					{
						weapDescription = weapDescription + tempPart + " ";
					} else if (loc) 
					{
						weapLoc = weapLoc + tempPart + " ";
					} else if (!name && !desc && !loc) weapDam = tempPart;

				}
				
				//Create weapon with damage, name, description, and location given
				int damage = atoi(weapDam.c_str());
				Weapon * tempWeapon = new Weapon(damage, weapName, weapDescription);

				gameMap.getRoomByName(weapLoc)->addThing(tempWeapon);	
				gameMap.getThings().push_back(tempWeapon);				

				
			//Create food if line starts with food
			} else if (tempName == "Food:")
			{
				//food requires name, description, health restored, and location (room or other bag)
				string foodName, foodDescription, foodLoc, foodHeal, tempPart;
				bool name = true;
				bool desc = false;
				bool loc = false;

				//Cycle through line until line ends
				while(iss>>tempPart) {
				
					if(tempPart == "Desc:")
					{
						name = false;
						desc = true;
						loc = false;
					} else if (tempPart == "Loc:" && !name && desc && !loc)
					{
						loc = true;
						name = false;
						desc = false;
					} else if (tempPart == "Heal:" && !name && !desc && loc)
					{

						loc = false;
						
					} else if (name)
					{

						foodName = tempPart;

					} else if (desc) 
					{
						foodDescription = foodDescription + tempPart + " ";
					} else if (loc) 
					{
						foodLoc = foodLoc + tempPart + " ";
					} else if (!name && !desc && !loc) foodHeal = tempPart;

				}
				
				//Create food with damage, name, description, and location given
				int heal = atoi(foodHeal.c_str());
				Food * tempFood = new Food(heal, foodName, foodDescription);

				gameMap.getRoomByName(foodLoc)->addThing(tempFood);	
				gameMap.getThings().push_back(tempFood);				


			//Create armor if line starts with armor
			} else if (tempName == "Armor:")
			{
				//armor requires name, description, health added, and location (room or other bag)
				string armorName, armorDescription, armorLoc, armorHeal, tempPart;
				bool name = true;
				bool desc = false;
				bool loc = false;

				//Cycle through line until line ends
				while(iss>>tempPart) {
				
					if(tempPart == "Desc:")
					{
						name = false;
						desc = true;
						loc = false;
					} else if (tempPart == "Loc:" && !name && desc && !loc)
					{
						loc = true;
						name = false;
						desc = false;
					} else if (tempPart == "Heal:" && !name && !desc && loc)
					{

						loc = false;
						
					} else if (name)
					{

						armorName = tempPart;

					} else if (desc) 
					{
						armorDescription = armorDescription + tempPart + " ";
					} else if (loc) 
					{
						armorLoc = armorLoc + tempPart + " ";
					} else if (!name && !desc && !loc) armorHeal = tempPart;

				}
				
				//Create food with damage, name, description, and location given
				int heal = atoi(armorHeal.c_str());
				Armor * tempArmor = new Armor(heal, armorName, armorDescription);

				gameMap.getRoomByName(armorLoc)->addThing(tempArmor);	
				gameMap.getThings().push_back(tempArmor);				



			}
		}
		
    	}

	return gameMap;
}


vector<character> Reader::readCharList(string charFile)
{
	vector<character> tempChars;

	//Stream-In input file
	string fileName = charFile;	
	ifstream infile(fileName.c_str());
	
	string line;

	while(getline(infile, line))
	{
		istringstream iss(line);
	
		string tempName;

		iss>>tempName;		

		if(tempName == "Name:")
		{
			bool name, desc, hp, damage;
			name = true;
			desc = hp = damage = false;

			string charName, charDesc, charHP, charDamage, charDodge, tempPart;

			while(iss>>tempPart) {
				if(tempPart=="Desc:")
				{
					name = false;
					desc = true;
				} else if (tempPart=="HP:")
				{
					desc = false;
					hp = true;
				} else if (tempPart=="Damage:")
				{
					hp = false;
					damage = true;
				} else if (tempPart=="Dodge:")
				{
					damage = false;
				} else if (name)
				{
					charName = tempPart;
				} else if (desc)
				{ 
					charDesc = charDesc + tempPart + " ";
				} else if (hp)
				{
					charHP = tempPart;
				} else if (damage)
				{
					charDamage = tempPart;
				} else charDodge = tempPart;
			}

			int hpInt = atoi(charHP.c_str());
			int damageInt = atoi(charDamage.c_str());
			int dodgeInt = atoi(charDodge.c_str());

		character tempChar = character(hpInt, damageInt, dodgeInt, charName, charDesc);
		
		tempChars.push_back(tempChar);

		
		} else
		{
			cout<<"Unable to read Character File."<<endl;
		}		

	}
		
	return tempChars;		
}



vector<Lifeform*> Reader::readMonsterList(string monsterFile, GameMap gameMap)
{
	//Initialize Monster vector
	vector<Lifeform*> tempMonsters;

	//Stream-In input file
	string fileName = monsterFile;	
	ifstream infile(fileName.c_str());


	string line;

	while(getline(infile, line))
	{
		istringstream iss(line);
	
		string tempName;

		iss>>tempName;		

		if(tempName == "Name:")
		{
			bool name, hp, damage;
			name = true;
			hp = damage = false;

			string monsterName, monsterHP, monsterDamage, monsterDodge, tempPart;

			while(iss>>tempPart) {
				if (tempPart=="HP:")
				{
					name = false;
					hp = true;
				} else if (tempPart=="Damage:")
				{
					hp = false;
					damage = true;
				} else if (tempPart=="Dodge:")
				{
					damage = false;
				} else if (name)
				{
					monsterName = tempPart; 
				} else if (hp)
				{
					monsterHP = tempPart;
				} else if (damage)
				{
					monsterDamage = tempPart;
				} else monsterDodge = tempPart;
			}

			int hpInt = atoi(monsterHP.c_str());
			int damageInt = atoi(monsterDamage.c_str());
			int dodgeInt = atoi(monsterDodge.c_str());


		Monster *  tempMonster = new Monster(hpInt, damageInt, dodgeInt, monsterName, gameMap.getRooms()[0]);
		gameMap.getThings().push_back(tempMonster);				
		tempMonsters.push_back(tempMonster);
    

		
		} else
		{
			cout<<"Unable to read Monster File."<<endl;
		}		

	}
	
	return tempMonsters;		
}

GameMap Reader::randEntityLoc(vector<Lifeform*> Entities, GameMap gameMap)
{
	srand(time(NULL));
	int mapSize = gameMap.getRooms().size();
	
	cout<<endl<<Entities.size()<<endl;
	
	for(int i = 0; i < Entities.size(); i++)
	{
		int location = rand()%mapSize;

		cout<<endl<<location<<endl;
		Entities[i]->move(gameMap.getRooms()[location]);

	}

	return gameMap;

} 


