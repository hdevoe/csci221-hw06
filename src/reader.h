#ifndef READER_H
#define READER_H

#include <sstream>
#include <string>
#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <vector>
#include "rooms/gamemap.h"
#include "rooms/room.h"
#include "items/bag.h"
#include "items/weapon.h"
#include "items/armor.h"
#include "items/food.h"

#include "entities/character.h"
#include "entities/monster.h"
#include "entities/lifeform.h"

using namespace std;

class Reader {
	public:
		Reader();
		GameMap readGameMap(string mapFile, GameMap gameMap);
		
		~Reader() {
			//delete all pointers
		}

		vector<character> readCharList(string charFile);
		vector<Lifeform*> readMonsterList(string monsterFile, GameMap gameMap);
		GameMap randEntityLoc(vector<Lifeform*> Entities, GameMap gameMap);	
	private:

};

#endif


