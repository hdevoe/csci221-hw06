#include "gamemap.h"
#include <iostream>
#include <map>
using namespace std;

Room* GameMap::getRoomByName(std::string roomName)
{
	for(unsigned int i = 0; i < gameRooms.size(); i++) {
		if(gameRooms[i]->getName() == roomName) return gameRooms[i];
	}

	return NULL;
}


void GameMap::makeRoom(std::string roomName, std::string roomDescription)
{
	Room* tempRoom = new Room(roomName, roomDescription);
	gameRooms.push_back(tempRoom);
}

void GameMap::setExit(std::string current, std::string other, std::string link)
{
	exits[getRoomByName(current)][link] = getRoomByName(other);
	
}

Room* GameMap::getExit(std::string currentRoom, std::string relation)
{
	return exits[getRoomByName(currentRoom)][relation];
}

bool GameMap::checkExit(std::string currentRoom, std::string relation)
{
	return exits.count(getRoomByName(currentRoom))&&exits[getRoomByName(currentRoom)].count(relation);
}
