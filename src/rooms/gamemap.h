#ifndef GAMEMAP_H
#define GAMEMAP_H

#include "room.h"
#include "../items/thing.h"
#include <map>
#include <utility>
#include <string>
#include <iostream>
#include <vector>

class GameMap {
    private:
        
	//2D vector of Rooms with a map of exits
	std::map<Room*, std::map<std::string, Room*> >  exits;
	

	//vector of rooms
	std::vector<Room*> gameRooms;	

	//vector of items
	std::vector<Thing*> allThings;
	
    public:

	//create room	
	void makeRoom(std::string, std::string);

	//set exit for room
        void setExit(std::string, std::string, std::string);

	//get exit for room
	Room* getExit(std::string, std::string);

	//check if exit exists for direction
	bool checkExit(std::string, std::string);

	//get room vector
	std::vector<Room*> getRooms() {return gameRooms;}

	//get item vector 
	std::vector<Thing*> getThings() {return allThings;}

	//get room by name
	Room* getRoomByName(std::string);

	//Delete all room pointers
	void clearGameMap()
	{
		for(unsigned int i = 0; i < gameRooms.size(); i++) {
			delete gameRooms[i];
		}

		for(unsigned int i = 0; i < allThings.size(); i++) {
			delete allThings[i];
		}
	}
};
#endif

