#ifndef ROOM_H
#define ROOM_H
#include <iostream>
#include "../items/container.h"

class Room : public Container {
	private:
	std::string longName;
	std::string description;
	public:
	Room():Container(){}
	Room(std::string name, std::string descr) : Container (name, descr){
		longName = name;
		description = descr;	
	}
	std::string getName();
	std::string getDescription();
};

#endif

